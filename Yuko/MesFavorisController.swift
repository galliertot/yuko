//
//  MesFavorisController.swift
//  Yuko
//
//  Created by Thomas Gallier on 06/04/2021.
//

import Foundation
import UIKit

class MesFavorisController:UITableViewController {
	
	var data = ["Toto", "Tati", "Tata", "Tatu", "Tota", "Tamere"]
	
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.register(UINib(nibName: "ViewTable", bundle: nil), forCellReuseIdentifier: "cellNormal")
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "cellNormal", for:indexPath) as! ViewTableController
		cell.configure(data: data[indexPath.row + indexPath.section * 2])
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("Coucou toto")
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 2
	}
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return Int(data.count / 2)
	}
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 50
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return "Section " + String(section)
	}
	
	
	
	
}
