//
//  ViewController.swift
//  Yuko
//
//  Created by Thomas Gallier on 06/04/2021.
//

import UIKit

class ViewController: UIViewController {
	
	@IBOutlet weak var viewAnimation: UIView!
	@IBOutlet weak var titleOfPage: UILabel!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(true)
		
	}
	
	@IBAction func changNameOfTitle(_ sender: Any) {
		UIView.animate(withDuration: 1, animations: {
			self.viewAnimation?.backgroundColor = .blue
		})
		self.titleOfPage?.text = "L'app de Mamel"
		
		Model.shared.request()
	}
	
	
}

