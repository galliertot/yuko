//
//  ViewTableController.swift
//  Yuko
//
//  Created by Thomas Gallier on 06/04/2021.
//

import Foundation
import UIKit

class ViewTableController:UITableViewCell {
	
	@IBOutlet weak var textCell: UILabel!
	@IBOutlet weak var imageCell: UIImageView!
	

	func configure(data : String) {
		self.textCell.text = data
	}
}
